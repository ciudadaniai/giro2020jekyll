---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---
<div id="about" class="uk-section section-grey-light-soft animated fadeIn delay-4s fast">
  <div class="uk-container uk-width-2-3@m uk-text-center" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2>O que é o GIRO: 2020</h2>
        <p class="font-lead">2020 marca o início de um novo ciclo. O que queremos fazer coletivamente? </p>
        <p>O contexto atual traz a urgência de ações no presente que disputem o futuro de nossos bairros, favelas e cidades da Região Metropolitana do Rio de Janeiro. O GIRO2020 é uma iniciativa da sociedade civil que busca fortalecer a democracia, antes, durante e depois das eleições, através de uma agenda propositiva, processos formativos e mobilização social.</p>
      </div>
    </div>
  </div>
</div>

<div class="uk-section section-grey-light-soft animated fadeIn delay-4s fast uk-visible@m">
  <div class="uk-container uk-width-5-6@m" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <h2 class="uk-text-center">Ações</h2>
    <div class="uk-grid-large uk-child-width-1-3@m uk-grid-small uk-flex uk-flex-center" uk-grid uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
      <div>
        <img data-src="../assets/images/giro-encontros.svg" class="giro-actions" uk-img>
        <h3>Encontros</h3>
        <p>Ciclo de encontros mensais aberto à participação da sociedade civil, lideranças sociais e pré-candidaturas para discutir estratégias de incidência da agenda de propostas nas eleições, apontando prioridades para as cidades, a partir dos contextos.</p>
      </div>
      <div>
        <img data-src="../assets/images/giro-formacao.svg" class="giro-actions" uk-img>
        <h3>Formação</h3>
        <p>Formação para pré-candidaturas, com conteúdos propositivos de políticas públicas com foco na Região Metropolitana do Rio de Janeiro, tendências do debate público, estratégias de comunicação nas redes e financiamento de campanhas.</p>
      </div>
      <div>
        <img data-src="../assets/images/giro-lideranca.svg" class="giro-actions" uk-img>
        <h3>Lab Liderança</h3>
        <p>Espaço de troca de ferramentas e estratégias entre lideranças dos territórios da Região Metropolitana do Rio de Janeiro.</p>
      </div>
    </div>
  </div>
</div>

<div class="uk-section section-grey-light-soft uk-text-center animated fadeIn delay-4s fast uk-hidden@m">
  <div class="uk-container uk-width-5-6@m" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <h2>Ações</h2>
    <div class="uk-grid-large uk-child-width-1-3@m uk-grid-small uk-flex uk-flex-center" uk-grid uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
      <div>
        <img data-src="../assets/images/giro-encontros.svg" class="giro-actions" uk-img>
        <h3>Encontros</h3>
        <p>Ciclo de encontros mensais aberto à participação da sociedade civil, lideranças sociais e pré-candidaturas para discutir estratégias de incidência da agenda de propostas nas eleições, apontando prioridades para as cidades, a partir dos contextos.</p>
      </div>
      <div>
        <img data-src="../assets/images/giro-formacao.svg" class="giro-actions" uk-img>
        <h3>Formação</h3>
        <p>Formação para pré-candidaturas, com conteúdos propositivos de políticas públicas com foco na Região Metropolitana do Rio de Janeiro, tendências do debate público, estratégias de comunicação nas redes e financiamento de campanhas.</p>
      </div>
      <div>
        <img data-src="../assets/images/giro-lideranca.svg" class="giro-actions" uk-img>
        <h3>Lab Liderança</h3>
        <p>Espaço de troca de ferramentas e estratégias entre lideranças dos territórios da Região Metropolitana do Rio de Janeiro.</p>
      </div>
    </div>
  </div>
</div>

<div id="program" class="uk-section section-grey-light-soft animated fadeIn">
  <div class="uk-container uk-width-2-3@m uk-text-center" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2>Formação Giro2020</h2>
        <p>Formação gratuita e inédita com conteúdos, estratégias e ferramentas para fortalecer as pré-candidaturas municipais da Região Metropolitana do Rio de Janeiro.</p>
        <a style="margin:0.5em" href="https://bit.ly/FormacaoGiro2020" class="uk-button button-color-1st" target="_blank">Inscreva-se aqui</a>
        <a style="margin:0.5em" href="../assets/docs/formacaogiro2020.pdf" class="uk-button button-black" target="_blank">Baixe o edital</a>
      </div>
    </div>
  </div>
</div>

<div id="agenda" class="uk-section section-grey-light-soft fix-section animated fadeIn">
  <div class="uk-container uk-width-2-3@m uk-text-center" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-flex uk-flex-center" uk-grid>
      <div class="uk-width-1-1">
        <h2>Agenda Rio</h2>
        <p style="text-align: justify">Nas ruas, favelas, bairros, associações, igrejas, terreiros, movimentos e coletivos, já existem muitos de nós reunidos, dialogando e criando propostas concretas para mudar a realidade de um transporte caro e de má qualidade, de violência contra as mulheres, de falta de vagas nas escolas e creches, de ausência de emprego, de prefeituras e serviços públicos mal geridos e tantos outros desafios enfrentados diariamente.</p>
        <p style="text-align: justify">A Agenda Rio se propõe justamente a reunir um conjunto de propostas de políticas públicas para responder a essa realidade desigual, em que as periferias vivem sem acesso à direitos básicos e os moradores de áreas mais privilegiadas usufruem de infraestrutura e bem-estar.</p>
        <p style="text-align: justify">Coordenada pela Casa Fluminense e elaborada em parceria com rede de parceiros, a nova edição 2020 da Agenda Rio aponta em que direção queremos girar as nossas pautas, com propostas em dez eixos temáticos (habitação, emprego, transporte, segurança, saneamento, saúde, educação, cultura, assistência social e gestão) e comprometida com quatro valores transversais (justiça econômica, justiça racial, justiça de gênero e justiça ambiental).</p>
        <p>Em breve disponível!</p>
      </div>
    </div>
  </div>
</div>

<div id="map" class="uk-height-viewport uk-flex uk-flex-left uk-inline uk-flex-middle uk-background-cover uk-light animated fadeIn" data-src="/assets/images/map-desktop.png" uk-img>
  <!-- Desktop -->
  <div class="uk-container uk-text-center" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-width-1-1">
      <h2>Mapa da desigualdade</h2>
      <h3>Quer conhecer a realidade das desigualdades na Região Metropolitana do Rio?<br>
      O Mapa da Desigualdade 2020 reúne 40 indicadores que retratam os desafios socioeconômicos na região, a partir de dados públicos.</h3>
      <p class="font-little-title">Acesse o site e faça download da publicação</p>
      <a href="https://casafluminense.org.br/mapa-da-desigualdade/" class="uk-button button-white" target="_blank">Acesse o site</a>
    </div>
  </div>
  <!-- End desktop -->
</div>

<div id="events" class="uk-section section-grey-light-soft fix-section animated fadeIn">
  <div class="uk-container uk-width-3-5@m" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <h2 class="uk-text-center" style="margin-bottom:1em">Eventos</h2>
    <div class="uk-grid-large uk-child-width-1-1@m uk-grid-small uk-flex uk-flex-center" uk-grid uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
      <div>
        <div class="uk-card uk-card-default">
            <div class="uk-card-header">
                <div class="uk-grid-small uk-flex-middle" uk-grid>
                    <div class="uk-width-expand">
                        <h3 class="uk-card-title uk-margin-remove-bottom">#Encontro Online: Eleições 2020 e a crise dos transportes nas cidades</h3>
                        <p class="uk-text-meta uk-margin-remove-top">
                          <span class="uk-margin-small-right" uk-icon="calendar"></span><time datetime="2016-08-06T18:00">06/08 18:00hrs</time>
                        </p>
                    </div>
                </div>
            </div>
            <div class="uk-card-body">
              <p>Quais os desafios de realizar eleições seguras e participativas nas cidades no contexto da pandemia? Vamos lançar a Formação GIRO2020 debatendo as medidas necessárias a serem tomadas, a partir da perspectiva da sociedade civil, com foco nas desigualdades estruturais da Região Metropolitana do Rio de Janeiro.</p>
            </div>
            <div class="uk-card-footer">
              <p>
                <span class="uk-margin-small-right" uk-icon="video-camera"></span>Transmissão pelo <a href="https://www.facebook.com/casafluminense" target="_blank">Facebook</a> e <a href="https://www.youtube.com/user/CasaFluminense" target="_blank">Youtube</a> da Casa Fluminense
              </p>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="contact" class="uk-height-viewport uk-flex uk-inline uk-flex-middle uk-background-cover uk-light animated fadeIn" data-src="/assets/images/hero-1-desktop.png" uk-img>
  <!-- Desktop -->
  <div class="uk-container uk-text-center" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 200; repeat: false">
    <div class="uk-width-1-1">
      <h2>Contato</h2>
      <h3>Mais informações?<br>
      Fale com a gente: <a href="mailto:contato@cidadaniainteligente.org" style="text-decoration: underline">contato@cidadaniainteligente.org</a></h3>
    </div>
  </div>
  <!-- End desktop -->
</div>
